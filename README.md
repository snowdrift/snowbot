# snowbot

repo for the maubot instance that will be on snowdrift matrix channel including custom plugins, etc


[quickstart guide](https://github.com/oflisback/maubot-quickstart)

Can reference [karma](https://github.com/maubot/karma) to create a stats plugin

can use the [reminder plugin](https://github.com/maubot/reminder) for meeting reminders 
  - ideally this will not need continued resubscription every week but can work immediately in that context
  
Can use [reactbot plugin](https://github.com/maubot/reactbot) for the !folks group ping

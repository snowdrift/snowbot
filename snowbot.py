# Copyright (C) 2021 Stephen Michel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from html import escape
from maubot import Plugin, MessageEvent
from maubot.handlers import command
from mautrix.types import (EventType, Format, MessageType, TextMessageEventContent)

class SnowBot(Plugin):
  excluded_users = {'@appservice:libera.chat','@ChanServ:libera.chat'}

  @command.new()
  # command.argument is not yet documented; this was written based on this usage:
  # https://github.com/maubot/reminder/blob/de01aa55650adefeef4b98bbc694fc4171ecf7b3/reminder/bot.py#L115-L119
  # pass_raw is poorly named, I think; it seems like it should be named pass_rest. Without it, the
  # bot will complain if you try to write more than one word after '!folks'
  @command.argument("message", pass_raw=True, required=False)
  async def folks(self, evt: MessageEvent, message: str) -> None:
    folks = await self.get_power_users(evt.room_id)
    msg = "Folks: ^" if message == '' else f"Folks: {message} -"
    names = ", ".join(folks.values())
    names_html = ", ".join(f"<a href='https://matrix.to/#/{user}'>{user}</a>"
                           for user in folks.keys())
    content = TextMessageEventContent(
        msgtype=MessageType.TEXT, body=f"{msg} {names}",
        format=Format.HTML, formatted_body=f"{escape(msg)} {names_html}")
    await self.client.send_message(evt.room_id, content)

  # Using power level instead of storing a list of folks per-room means that we don't have to worry
  # about persisting a list or - more importantly - creating an interface for managing who's on it,
  # especially permissions around who's allowed to add or remove themselves.
  async def get_power_users(self, room_id: str):
    """
    Finds all users with power level greater than zero.

    Returns a dict of { username:displayname }.
    """
    state = await self.client.get_state(room_id)
    members = await self.client.get_joined_members(room_id)

    # We only expect one power level event, but that's not strictly guaranteed by the api, so merge
    # users from all the power level events. Also, we need to filter out some bad results:
    # - Users who previously had a non-default power level and then was demoted back to zero
    # - Hard-coded list of known bridges and bots
    # - Users who have left the room but still have a power level (usually admins)
    return { member:details.displayname
        for e in state if e.type == EventType.ROOM_POWER_LEVELS
        for (user,level) in e.content.users.items() if level > 0 and user not in self.excluded_users
        for (member,details) in members.items() if user == member }
